import {Component, OnInit} from '@angular/core';
import {ProductService} from "./core/services/product.service";
import {AuthenticationService} from "./core/services/authentication.service";
import {SpinnerService} from "./core/services/spinner.service";
import {TranslateService} from "@ngx-translate/core";
import {TokenService} from "./core/services/token.service";
import {I18nService} from "./core/services/i18n.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  title = 'Template driven forms';

  constructor(private productService: ProductService
              , private authService: AuthenticationService
              , private spinner: SpinnerService
              , private translate: TranslateService
              , private i18nService: I18nService) {
    this.translate.setDefaultLang(i18nService.getLang() ? i18nService.getLang().toLowerCase() : 'en');
  }

  ngOnInit() {
    this.spinner.showSpinner();
    setTimeout(() => {
      this.spinner.hideSpinner();
    }, 1000);
    // this.productService.searchProducts(new ProductForm()).subscribe(
    //   value => {
    //     console.log('-----', value);
    //   },
    //   error => {
    //     console.log('-----', error);
    //   }
    // );

    // let cred: Credential = new Credential();
    // cred.username = 'phiennv';
    // cred.password = '123456';
    //
    // this.authService.doLogin(cred).subscribe(
    //   value => {
    //     console.log('-------------------------', value);
    //   }
    // );

  }
}
