import {Injectable} from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class TokenService {
  constructor() {
  }

  public clearToken() {
      localStorage.removeItem('accessToken');
      localStorage.removeItem('refreshToken');
  }

  public getAccessToken(): string | null {
    let accessToken = localStorage.getItem('accessToken');
    return accessToken || null;
  }

  public setAccessToken(accessToken: string):void {
    localStorage.setItem('accessToken', accessToken);
  }

  public getRefreshToken(): string | null {
    let accessToken = localStorage.getItem('refreshToken');
    return accessToken || null;
  }

  public setRefreshToken(refreshToken: string):void {
    localStorage.setItem('refreshToken', refreshToken);
  }

}
