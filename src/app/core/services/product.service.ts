import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {ProductForm} from "../model/product.form";
import {environment} from "../../../environment/environment";
import {TokenService} from "./token.service";

@Injectable({
  providedIn: "root"
})
export class ProductService {
  constructor(private http: HttpClient, private tokenService: TokenService) {
  }

  getProductById(id: number): Observable<any> {
    return this.http.get(`${environment.productUrl}/${id}`);
  }

  searchProducts(product:ProductForm): Observable<any[]> {
    const body=JSON.stringify(product);
    return this.http.post<any[]>(`${environment.productUrl}/search`, body);
  }

  createProduct(product:ProductForm): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.append('Authorization', `${this.tokenService.getAccessToken()}`);
    const body=JSON.stringify(product);
    return this.http.post(`${environment.productUrl}/create`, body, { headers: headers});
  }

  updateProduct(product:ProductForm): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.append('Authorization', `${this.tokenService.getAccessToken()}`);
    const body=JSON.stringify(product);
    return this.http.put(`${environment.productUrl}/update`, body, { headers: headers});
  }

  deleteProducts(ids: number[]): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.append('Authorization', `${this.tokenService.getAccessToken()}`);
    const body=JSON.stringify(ids);
    // @ts-ignore
    return this.http.delete(`${environment.productUrl}/delete`, body, { headers: headers});
  }
}
