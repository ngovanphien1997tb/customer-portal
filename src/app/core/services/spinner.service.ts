import {Injectable} from "@angular/core";
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable({
    providedIn: 'root'
})
export class SpinnerService {
    private isShow: boolean;

    constructor(private spinner: NgxSpinnerService) {
        this.isShow = false;
    }

    showSpinner() {
        if (!this.isShow) {
            setTimeout(() => {
                this.spinner.show();
            }, 100);
            this.isShow = true;
        }
    }

    hideSpinner() {
        if (this.isShow) {
            setTimeout(() => {
                this.spinner.hide();
            }, 100);
            this.isShow = false;
        }
    }

    disableSpinner() {
        this.isShow = true;
    }
}