import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Credential} from "../model/credential";
import {environment} from "../../../environment/environment";
import {RegisterForm} from "../model/register.form";

@Injectable(
  {
    providedIn: 'root'
  }
)
export class AuthenticationService {
  constructor(private http: HttpClient) {
  }

  public doLogin(credential: Credential):Observable<any> {
    const body=JSON.stringify(credential);
    return this.http.post(`${environment.authUrl}/sign-in`, body);
  }

  public doRefreshToken(refreshToken: string): Observable<any> {
    const body=JSON.stringify(refreshToken);
    return this.http.post(`${environment.authUrl}/refresh-token`, body);
  }

  public registerAccount(registerForm: RegisterForm): Observable<any> {
    const body = JSON.stringify(registerForm);
    return this.http.post(`${environment.authUrl}/register`, body);
  }
}
