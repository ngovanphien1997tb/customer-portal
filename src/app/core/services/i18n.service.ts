import { Injectable } from '@angular/core';
import {TranslateService} from "@ngx-translate/core";

@Injectable({
  providedIn: 'root'
})
export class I18nService {

  constructor(private translateService: TranslateService) { }

  getLang(): string {
    const data = localStorage.getItem('lang');
    return data||'';
  }

  setLang(lang: string): void {
    localStorage.setItem('lang', lang);
    this.translateService.setDefaultLang(lang.toLowerCase());
  }
}
