import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../environment/environment";
import {TokenService} from "./token.service";

@Injectable(
  {
    providedIn: 'root'
  }
)
export class FileService {

  constructor(private http:HttpClient, private tokenService: TokenService) {
  }

  public uploadImage(file: File):Observable<string> {

    const formData = new FormData();
    formData.append("multipartFile", file);

    let headers = new HttpHeaders();
    headers = headers.append('Authorization', `${this.tokenService.getAccessToken()}`);
    return this.http.post<string>(`${environment.productUrl}/upload/image`, formData);
  }
}
