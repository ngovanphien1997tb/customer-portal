import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {TokenService} from "../services/token.service";
import {AuthenticationService} from "../services/authentication.service";

@Injectable({
	providedIn: 'root',
})
export class NoAuthGuard implements CanActivate {
	constructor(
		private authenticationService: AuthenticationService,
    private token: TokenService,
    private router: Router,
	) {
	}

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
		return this.checkCondition();
	}

	private checkCondition(): boolean {
    const accessToken = this.token.getAccessToken();
    if (!accessToken) {
      return true;
    }else {
      this.router.navigate(['home']);
      return false;
    }
	}
}
