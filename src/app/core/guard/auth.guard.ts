import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {TokenService} from "../services/token.service";

@Injectable({
	providedIn: 'root',
})
export class AuthGuard implements CanActivate {
	constructor(
		private token: TokenService,
		private router: Router,
	) {
	}

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
		return this.checkCondition();
	}

	private checkCondition(): boolean {
		const accessToken = this.token.getAccessToken();
		if (accessToken) {
			return true;
		}
		this.router.navigate(['/login']).then();
		return false;
	}
}
