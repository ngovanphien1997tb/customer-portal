export class ProductForm {
  name!: string;
  imagesUrl!: string;
  price!: number;
  rating!: number;
  describe!: string;
  quantity!: string;
  listOfColor!: string;
}
