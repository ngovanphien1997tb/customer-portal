export class Contact {
  // @ts-ignore
  firstname: string;
  // @ts-ignore
  lastname: string;
  // @ts-ignore
  email: string;
  // @ts-ignore
  gender: string;
  // @ts-ignore
  isMarried: boolean;
  // @ts-ignore
  country: string;
  // @ts-ignore
  address: {
    city: string;
    street: string;
    pincode: string;
  }
}
