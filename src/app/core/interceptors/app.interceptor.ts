import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from "rxjs";
import {Router} from "@angular/router";
import {I18nService} from "../services/i18n.service";

@Injectable()
export class AppInterceptor implements HttpInterceptor {

  constructor(private router: Router, private i18nService: I18nService) {
  }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let httpHeaders = new HttpHeaders();
    httpHeaders = httpHeaders.append('content-type','application/json');
    httpHeaders = httpHeaders.append('lang',  this.i18nService.getLang().toLowerCase());
    req = req.clone(
      {
        headers: httpHeaders
      }
    );
    return next.handle(req);
  }
}
