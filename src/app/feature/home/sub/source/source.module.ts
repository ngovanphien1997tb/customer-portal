import {NgModule} from '@angular/core';
import {AboutComponent} from "./about/about.component";
import {BlogComponent} from "./blog/blog.component";
import {ContactComponent} from "./contact/contact.component";
import {FaIconLibrary, FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {
  faArrowRight,
  faCartShopping, faEnvelope,
  faEye,
  faEyeSlash, faFilter, faHeart,
  faKey, faLocationDot,
  faMagnifyingGlass, faPhone,
  faUser,
  faUserPlus, faXmark
} from "@fortawesome/free-solid-svg-icons";
import {ShareModule} from "../../../../share/share.module";


@NgModule({
  declarations: [
    AboutComponent,
    BlogComponent,
    ContactComponent
  ],
  imports: [
    FontAwesomeModule,
    ShareModule
  ],
  exports: [
    AboutComponent,
    BlogComponent,
    ContactComponent
  ]
})
export class SourceModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(
      faUser,
      faKey,
      faUserPlus,
      faEye,
      faEyeSlash,
      faMagnifyingGlass,
      faCartShopping,faLocationDot,faPhone, faEnvelope,faArrowRight,faXmark,
      faHeart, faFilter
    );
  }
}
