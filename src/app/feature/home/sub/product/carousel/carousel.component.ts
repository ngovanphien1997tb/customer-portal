import {AfterViewInit, Component, ElementRef, Renderer2, ViewChild} from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css']
})
export class CarouselComponent implements AfterViewInit{

  // @ts-ignore
  // @ViewChild('captionCarousel1', {static:true, read: ElementRef}) captionCarousel1: ElementRef;
  // // @ts-ignore
  // @ViewChild('captionCarousel2', {static:true, read: ElementRef}) captionCarousel2: ElementRef;

  constructor(private render: Renderer2) {
  }
  ngAfterViewInit() {
    // this.render.setStyle(this.captionCarousel1.nativeElement, 'opacity', '1');
    // this.render.setStyle(this.captionCarousel1.nativeElement, 'top', '50%');
    // this.render.setStyle(this.captionCarousel2.nativeElement, 'opacity', '1');
    // this.render.setStyle(this.captionCarousel2.nativeElement, 'top', '50%');
  }
}
