import {NgModule} from '@angular/core';
import {ProductsComponent} from "./products/products.component";
import {ProductsPreviewComponent} from "./products.preview/products.preview.component";
import {CarouselComponent} from "./carousel/carousel.component";
import {ShareModule} from "../../../../share/share.module";
import {CarouselModule} from "ngx-bootstrap/carousel";
import {FaIconLibrary, FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {
  faArrowRight,
  faCartShopping, faEnvelope,
  faEye,
  faEyeSlash, faFilter, faHeart,
  faKey, faLocationDot,
  faMagnifyingGlass, faPhone,
  faUser,
  faUserPlus, faXmark
} from "@fortawesome/free-solid-svg-icons";
import {NgClass} from "@angular/common";
import {TranslateModule} from "@ngx-translate/core";


@NgModule({
  declarations: [
    CarouselComponent,
    ProductsComponent,
    ProductsPreviewComponent
  ],
  exports: [
    CarouselComponent,
    ProductsPreviewComponent,
    ProductsComponent
  ],
    imports: [
        ShareModule,
        CarouselModule,
        FontAwesomeModule,
        NgClass,
        TranslateModule
    ]
})
export class ProductModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(
      faUser,
      faKey,
      faUserPlus,
      faEye,
      faEyeSlash,
      faMagnifyingGlass,
      faCartShopping,faLocationDot,faPhone, faEnvelope,faArrowRight,faXmark,
      faHeart, faFilter
    );
  }
}
