import {Component, ElementRef, HostListener, Renderer2, ViewChild} from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent {

  isShowFilter:boolean = false;
  isShowSearch: boolean = false;

  // @ts-ignore
  @ViewChild('products', {static: true, read: ElementRef}) products: ElementRef;
  // @ts-ignore
  @ViewChild('btnFilter', {static: true, read: ElementRef}) btnFilter: ElementRef;
  // @ts-ignore
  @ViewChild('btnSearch', {static: true, read: ElementRef}) btnSearch: ElementRef;
  // @ts-ignore
  @ViewChild('filterForm', {static: true, read: ElementRef}) filterForm: ElementRef;
  // @ts-ignore
  @ViewChild('searchForm', {static: true, read: ElementRef}) searchForm: ElementRef;

  constructor(private render: Renderer2) {

  }

  @HostListener('window:resize', ['$event'])
  onResize(event: Event) {
    if(this.isShowFilter == true) {
      this.onViewportChange();
    }

  }

  showFilterForm() {
    if(this.isShowSearch == true ) {
      this.isShowSearch = false;
    }
    this.isShowFilter = !this.isShowFilter;

    if (this.isShowFilter == true) {
      this.render.setStyle(this.searchForm.nativeElement, 'z-index', '0');
      this.render.setStyle(this.filterForm.nativeElement, 'z-index', '1');
      this.render.setStyle(this.filterForm.nativeElement, 'display', 'block');
      this.onViewportChange();
    } else {
      this.render.removeStyle(this.products.nativeElement, 'margin-top');
    }
  }

  onViewportChange() {
    let viewportWidth = window.innerWidth || document.documentElement.clientWidth;
    if(viewportWidth < 1200 && viewportWidth > 1000) {
      this.render.setStyle(this.products.nativeElement, 'margin-top', '700px');
    } else if(viewportWidth <= 1000) {
      this.render.setStyle(this.products.nativeElement, 'margin-top', '1200px');
    }
    else {
      this.render.setStyle(this.products.nativeElement, 'margin-top', '440px');
    }
  }

  showSearchForm() {
    if(this.isShowFilter == true) {
      this.isShowFilter = false;
    }
    this.isShowSearch = !this.isShowSearch;

    if (this.isShowSearch == true) {
      this.render.setStyle(this.searchForm.nativeElement, 'z-index', '1');
      this.render.setStyle(this.filterForm.nativeElement, 'display', 'none');
      this.render.setStyle(this.products.nativeElement, 'margin-top', '150px');
    } else {
      this.render.removeStyle(this.products.nativeElement, 'margin-top');
    }
  }
}
