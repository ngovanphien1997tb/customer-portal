import {NgModule} from '@angular/core';
import {SourceModule} from "./source/source.module";
import {ProductModule} from "./product/product.module";

@NgModule({
  declarations: [],
  imports: [
    ProductModule,
    SourceModule
  ],
  exports: [
    ProductModule,
    SourceModule
  ]
})
export class SubModule { }
