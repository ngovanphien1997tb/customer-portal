import {NgModule} from '@angular/core';
import {SubModule} from "./sub/sub.module";
import {HomeIndexComponent} from "./home.index/home.index.component";
import {HomeRoutingModule} from "./home.routing.module";
import {ShareModule} from "../../share/share.module";
import {HomeSubComponent} from "./home.sub/home.sub.component";
import {ShopComponent} from "./shop/shop.component";
import {ProfileComponent} from "./profile/profile.component";


@NgModule({
  declarations: [
    HomeIndexComponent,
    HomeSubComponent,
    ShopComponent,
    ProfileComponent
  ],
  imports: [
    SubModule,
    HomeRoutingModule,
    ShareModule
  ]
})
export class HomeModule { }
