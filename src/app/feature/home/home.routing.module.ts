import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {HomeIndexComponent} from "./home.index/home.index.component";
import {HomeSubComponent} from "./home.sub/home.sub.component";
import {CartComponent} from "../../share/layout";
import {BlogComponent} from "./sub/source/blog/blog.component";
import {AboutComponent} from "./sub/source/about/about.component";
import {ContactComponent} from "./sub/source/contact/contact.component";
import {ShopComponent} from "./shop/shop.component";
import {ProfileComponent} from "./profile/profile.component";

export const routes: Routes = [
  {
    path: 'home',
    component: HomeIndexComponent,
    children: [
      {
        path: 'index',
        component: HomeSubComponent,
        data: {
          isIndexPage: true
        }
      },
      {
        path: '',
        component: HomeSubComponent,
        data: {
          isIndexPage: true
        }
      },
      {
        path: 'shop',
        component: ShopComponent
      },
      {
        path: 'feature',
        component: CartComponent
      },
      {
        path: 'blog',
        component: BlogComponent
      },
      {
        path: 'about',
        component: AboutComponent
      },
      {
        path: 'contact',
        component: ContactComponent
      },
      {
        path: 'profile',
        component: ProfileComponent
      },
      {
        path: '**',
        redirectTo: '/404'
      }
    ]
  }
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class HomeRoutingModule {
}
