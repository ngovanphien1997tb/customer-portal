import { Component } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthenticationService} from "../../../core/services/authentication.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  registerForm: FormGroup;
  messageError: any;
  constructor(private fb:FormBuilder, private authService: AuthenticationService,
              public router: Router) {
    this.registerForm = this.fb.group({
      firstname: ['', [Validators.required]],
      lastname: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', [Validators.required]],
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
      repassword: ['', [Validators.required]]
    });
  }

  onSubmit() {
    console.log('----------------', this.registerForm.value);
    let dataForm = this.registerForm.value;
    let user = {
      firstName: dataForm.firstName,
      lastName:dataForm.lastName,
      username:dataForm.username,
      password:dataForm.password,
      email:dataForm.email,
      dob:"11/07/2000",
      address:"Thai binh",
      numberPhone: dataForm.phone
    }

    this.authService.registerAccount(user).subscribe(
      value => {

        this.router.navigate(['login']);

      },
      error => {
        console.log('--------------------', error.error.messages);
        this.messageError = error.error.messages;
      }
    );
  }
}
