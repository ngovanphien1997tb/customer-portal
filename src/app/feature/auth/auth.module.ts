import {NgModule} from '@angular/core';
import {LoginComponent} from "./login/login.component";
import {AuthRoutingModule} from "./auth-routing.module";
import {FaIconLibrary, FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {
  faCakeCandles, faCalendarDay,
  faEnvelope,
  faEye,
  faEyeSlash,
  faKey,
  faPhone,
  faSignature,
  faUser,
  faUserPlus
} from "@fortawesome/free-solid-svg-icons";
import {ReactiveFormsModule} from "@angular/forms";
import {JsonPipe, NgIf} from "@angular/common";
import { RegisterComponent } from './register/register.component';

@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
  ],
  imports: [
    AuthRoutingModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    NgIf,
    JsonPipe
  ]
})
export class AuthModule {

  constructor(library: FaIconLibrary) {
    library.addIcons(
      faUser,
      faUserPlus,
      faEye,
      faKey,
      faEyeSlash,
      faSignature,
      faEnvelope,
      faPhone,
      faCakeCandles,
      faCalendarDay
    );
  }
}
