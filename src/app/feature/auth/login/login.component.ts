import { Component } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthenticationService} from "../../../core/services/authentication.service";
import {TokenService} from "../../../core/services/token.service";
import {Router} from "@angular/router";
import {SpinnerService} from "../../../core/services/spinner.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  // @ts-ignore
  loginForm: FormGroup;
  passwordHidden: boolean = true;
  messageError: any;
  constructor(private fb:FormBuilder
              , private authService: AuthenticationService
              , private tokenService: TokenService
              , private router: Router
              , private spinner: SpinnerService) {
    // @ts-ignore
    this.loginForm = this.fb.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  onSubmit() {
    console.log("--------------", this.loginForm.value);
    let username = this.loginForm.get('username')?.value;
    let password = this.loginForm.get('password')?.value;
    let cred = {
      username: username,
      password: password
    };

    this.spinner.showSpinner()
    this.authService.doLogin(cred).subscribe(
      value => {
        this.tokenService.setAccessToken(value.data.accessToken);
        this.tokenService.setRefreshToken(value.data.refreshToken);
        this.router.navigate(['home','profile']);
        this.spinner.hideSpinner();
      },
      error => {
        console.log('------------', error.error.messages);
        this.messageError = error.error.messages;
        this.spinner.hideSpinner();
      }
    );
  }

  onRegister() {
    this.router.navigate(['register'])
  }
}
