import {AbstractControl, FormControl, ValidationErrors, ValidatorFn} from "@angular/forms";
const PHONE_PATTERN = "^[0-9]{10}$";
const PASSWORD_PATTERN = "^(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d@$!%*?&~^#(){};:,./\\\\[\\]|+-_=`'\"><]{2,}$";
export function phoneNumberValidator(): ValidatorFn {
	return (control: AbstractControl) : ValidationErrors | null  => {
		const value = control.value;
		if (!value) {
			return {required: true};
		}
		if(!new RegExp(PHONE_PATTERN).test(value)) {
			control.markAsDirty();
			if(value.length != 10) {
				return {invalidLengthPhoneNumber: true};
			}
			return {invalidPhoneNumber: true};
		}
		return null;
	}
}

export function formatPasswordValidator() {
	return (control: FormControl) : ValidationErrors | null  => {
		const value = control.value;
		if (!value) {
			return {required: true};
		}
		if(!new RegExp(PASSWORD_PATTERN).test(value)) {
			control.markAsDirty();
			return {invalidFormatPassword: true};
		}
		return null;
	}
}

