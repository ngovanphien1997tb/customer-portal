import {NgModule} from '@angular/core';
import {LayoutModule} from "./layout/layout.module";
import {UtilModule} from "./util/util.module";
import {CommonModule} from "@angular/common";
import {TtIfDirective} from './directive/tt-if.directive';
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    TtIfDirective
  ],
  imports: [
    LayoutModule,
    UtilModule,
    CommonModule,
    FormsModule
  ],
  exports:[
    LayoutModule,
    UtilModule,
    CommonModule,
    FormsModule,
    TtIfDirective
  ]
})
export class ShareModule {
}
