import {NgModule} from '@angular/core';
import {CartComponent, FooterComponent, HeaderComponent, NavbarComponent} from "./index";
import {FaIconLibrary, FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {faCartShopping, faHeart, faList, faMagnifyingGlass, faXmark} from "@fortawesome/free-solid-svg-icons";
import {RouterLink, RouterLinkActive} from "@angular/router";
import { Navbar1Component } from './navbar1/navbar1.component';
import {NgIf} from "@angular/common";
import { HearderResponesComponent } from './hearder-respones/hearder-respones.component';
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  declarations: [
    CartComponent,
    FooterComponent,
    HeaderComponent,
    NavbarComponent,
    Navbar1Component,
    HearderResponesComponent
  ],
    imports: [
        FontAwesomeModule,
        RouterLink,
        RouterLinkActive,
        NgIf,
        TranslateModule
    ],
  exports: [
    CartComponent,
    FooterComponent,
    HeaderComponent,
    NavbarComponent,
    Navbar1Component
  ]
})
export class LayoutModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(
      faMagnifyingGlass,
      faCartShopping,
      faHeart,
      faList,
      faXmark
    )
  }
}
