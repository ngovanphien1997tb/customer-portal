import { Component } from '@angular/core';
import {I18nService} from "../../../core/services/i18n.service";

@Component({
  selector: 'app-hearder-respones',
  templateUrl: './hearder-respones.component.html',
  styleUrls: ['./hearder-respones.component.css']
})
export class HearderResponesComponent {
  lang:string;
  constructor(private i18n: I18nService) {
    this.lang = i18n.getLang()||'EN';
  }

  setLang(lang: string) {
    this.lang = lang;
    this.i18n.setLang(lang);
  }
}
