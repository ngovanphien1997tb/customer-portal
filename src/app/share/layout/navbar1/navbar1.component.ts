import {Component, HostListener} from '@angular/core';

@Component({
  selector: 'app-navbar1',
  templateUrl: './navbar1.component.html',
  styleUrls: ['./navbar1.component.css']
})
export class Navbar1Component {

  showNavbar: boolean = false;

  onClickShowNavBar() {
    this.showNavbar = !this.showNavbar;
  }
  @HostListener('window:scroll', ['$event'])
  onWindowScroll(event: Event) {
    let viewportWidth = window.innerWidth || document.documentElement.clientWidth;
    if(this.showNavbar && viewportWidth > 1200) {
      let header = document.getElementById('header');
      let headerPosition = header?.offsetTop;

      let navbar = document.getElementById('navbar');
      let navbarPosition = navbar?.offsetTop;
      // @ts-ignore
      if(window.pageYOffset >= navbarPosition && window.pageYOffset > headerPosition) {
        // @ts-ignore
        navbar.classList.add('sticky-navbar');
        // console.log('window page Y offset :', window.pageYOffset, 'sticky position: ', navbarPosition )
      } else {
        // @ts-ignore
        navbar.classList.remove('sticky-navbar');
      }
    }
  }
}
