import {AfterViewInit, Component, HostListener, Input, OnInit, Renderer2, ViewChild} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {BehaviorSubject} from "rxjs";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, AfterViewInit{

  showNavbar: boolean = false;

  constructor(private render: Renderer2) {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
  }

  onClickShowNavBar() {
    this.showNavbar = !this.showNavbar;
  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll(event: Event) {
    let viewportWidth = window.innerWidth || document.documentElement.clientWidth;
    if (this.showNavbar && viewportWidth > 1200) {
      let header = document.getElementById('header');
      let headerPosition = header?.offsetTop;

      let navbar = document.getElementById('navbar');
      let navbarPosition = navbar?.offsetTop;
      // @ts-ignore
      if (window.pageYOffset >= navbarPosition && window.pageYOffset > headerPosition) {
        // @ts-ignore
        navbar.classList.add('sticky-navbar');
        // console.log('window page Y offset :', window.pageYOffset, 'sticky position: ', navbarPosition )
      } else {
        // @ts-ignore
        navbar.classList.remove('sticky-navbar');
      }
    }
  }
}
