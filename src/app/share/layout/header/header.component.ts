import {Component, OnInit} from '@angular/core';
import {I18nService} from "../../../core/services/i18n.service";
import {AuthenticationService} from "../../../core/services/authentication.service";
import {TokenService} from "../../../core/services/token.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit{

  lang:string;

  loginStatus: boolean = true;
  constructor(private i18n: I18nService
              , private authService: AuthenticationService
              , private tokenService: TokenService
              , private router: Router) {
    this.lang = i18n.getLang()||'EN';

  }

  ngOnInit() {
    let accessToken = this.tokenService.getAccessToken();
    if(!accessToken) {
      this.loginStatus = true;
    }else {
      this.loginStatus = false;
    }
  }

  onLogout() {
    this.tokenService.clearToken();
    this.loginStatus = false;
    this.router.navigate(['login']);
  }

  onLogin() {
    let accessToken = this.tokenService.getAccessToken();
    if(!accessToken) {
      this.router.navigate(['login'])
    } else {
      this.router.navigate(['home','profile']);
    }
  }

  setLang(lang: string) {
    this.lang = lang;
    this.i18n.setLang(lang);

  }
}
