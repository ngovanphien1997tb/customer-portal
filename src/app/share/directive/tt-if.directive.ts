import {Directive, Input, TemplateRef, ViewContainerRef} from '@angular/core';

@Directive({
  selector: '[ttIf]'
})
export class TtIfDirective {

  // @ts-ignore
  _ttif: boolean;

  constructor(private _viewContainer: ViewContainerRef,
              private templateRef: TemplateRef<any>) {
  }


  @Input()
  set ttIf(condition: boolean) {
    this._ttif = condition
    console.log("container view :", this._viewContainer);
    console.log('template:', this.templateRef)
    this._updateView();
  }

  _updateView() {
    if (this._ttif) {
      this._viewContainer.createEmbeddedView(this.templateRef);
      this._viewContainer.createEmbeddedView(this.templateRef);
    } else {
      this._viewContainer.clear();
    }
  }
}
