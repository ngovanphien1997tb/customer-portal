import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FilterComponent} from "./filter/filter.component";
import {NotFoundComponent} from "./not.found/not.found.component";
import {SearchComponent} from "./search/search.component";
import {FaIconLibrary, FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {
  faArrowRight,
  faCartShopping, faEnvelope,
  faEye,
  faEyeSlash, faFilter, faHeart,
  faKey, faLocationDot,
  faMagnifyingGlass, faPhone,
  faUser,
  faUserPlus, faXmark
} from "@fortawesome/free-solid-svg-icons";



@NgModule({
  declarations: [
    FilterComponent,
    NotFoundComponent,
    SearchComponent
  ],
  imports: [
    FontAwesomeModule
  ],
  exports: [
    FilterComponent,
    NotFoundComponent,
    SearchComponent
  ]
})
export class UtilModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(
      faUser,
      faKey,
      faUserPlus,
      faEye,
      faEyeSlash,
      faMagnifyingGlass,
      faCartShopping,faLocationDot,faPhone, faEnvelope,faArrowRight,faXmark,
      faHeart, faFilter
    );
  }
}
