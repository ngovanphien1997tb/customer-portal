export const environment = {
  production: true,
  productUrl: 'http://3.27.240.5:8765/api-product',
  authUrl: 'http://3.27.240.5:8765/api-auth'
}
// gateway: 3.27.240.5
// product: 3.26.196.140
// auth: 54.252.243.72
// naming: 3.27.207.230
// config: 54.206.160.13
// jenkin: 3.27.216.71
