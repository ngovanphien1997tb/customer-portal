FROM node:18.17-alpine AS build
LABEL authors="phiennv"

WORKDIR /dist/src/app

RUN npm cache clean --force

COPY package*.json .

RUN npm install

COPY . .

RUN npm run build --prod

FROM nginx:latest

COPY --from=build /dist/src/app/dist/customer-portal /usr/share/nginx/html
COPY /nginx.conf  /etc/nginx/conf.d/default.conf

EXPOSE 80
